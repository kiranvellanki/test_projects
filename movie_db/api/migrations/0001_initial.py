# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Movies',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('popularity', models.CharField(max_length=500, null=True)),
                ('director', models.CharField(max_length=500, null=True)),
                ('genre', models.CharField(max_length=500, null=True)),
                ('movielist_score', models.CharField(max_length=500, null=True)),
                ('name', models.CharField(max_length=500, null=True)),
                ('dt_added', models.DateTimeField(auto_now_add=True, null=True)),
                ('dt_updated', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
    ]
