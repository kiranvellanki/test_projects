from django.contrib import admin

# Register your models here.
from .models import Movies

class MoviesAdmin(admin.ModelAdmin):
    list_display = ('name', 'director', 'genre', 'movielist_score', 'popularity')
    search_fields = ('name', 'director')

admin.site.register(Movies, MoviesAdmin)
