import json
import MySQLdb

HOST = 'localhost'
USER = 'root'
PASSWORD = 'bujitalli'
DATABASE = 'movielist'

db = MySQLdb.connect(host=HOST, user=USER, passwd=PASSWORD, db=DATABASE)
cursor = db.cursor()

movies_list = open('imdb.json', 'r').read()
data = json.loads(movies_list)

for rec in data:
    query = "INSERT INTO api_movies (popularity, director, genre, movielist_score, name) values('%s', '%s', '%s', '%s', '%s')"
    popularity, director, genre, movielist_score, name = rec['99popularity'],\
                rec['director'], rec['genre'], rec['movielist_score'], rec['name']
    strip_list = [item.strip() for item in genre]
    values = (popularity, director, '<>'.join(strip_list), movielist_score, name)
    cursor.execute(query%values)
    db.commit()
