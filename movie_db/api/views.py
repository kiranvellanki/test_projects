from django.shortcuts import render
from django.db.models import Q
from django.http import HttpResponse, HttpResponseNotFound
from rest_framework.response import Response
from rest_framework.decorators import api_view

import json

from models import Movies

def test(request):
    return render(request, 'test.html', {})

def movies(request):
    movies_list = []
    movies = Movies.objects.all()
    for movie in movies:
        movies_list.append(movie.__dict__)

    data = {}
    data['status'] = 200
    data['message'] = 'Success'
    data['movie_list'] = movies_list
    return HttpResponse(movies_list, content_type="application/json")

def latest(request):
    this_movie = Article.objects.first()
    if this_movie:
        context = {
            'popularity':this_movie.popularity,
            'director' : this_movie.director,
            'genre' : this_movie.genre.name if this_movie.genre else '',
            'movielist_score' : this_movie.movielist_score,
            'name' : this_movie.name.url if this_movie.name else None
        }
        return render(request, 'all.html', context)
    else:
        return HttpResponseNotFound("Article not found")

def article_details(request, pk):
    #pk =1
    ###print movie_id
    this_movie = Article.objects.filter(id=pk).first()
    if this_movie:
        context = {
            'popularity':this_movie.popularity,
            'director' : this_movie.director,
            'genre' : this_movie.genre.name if this_movie.genre else '',
            'movielist_score' : this_movie.movielist_score,
            'name' : this_movie.name.url if this_movie.name else False
        }
        return render(request, 'all.html', context)
    else:
        return HttpResponseNotFound("movies not found")


@api_view(['GET'])
def allMovies(request):
    all_movies = movies.objects.all().values()
    return Response(all_movies)

@api_view(['GET'])
def movieDetails(request, pk):
    if Movies.objects.filter(pk=pk).exists():
        movie = Movies.objects.filter(pk=pk).values('popularity', 'director', 'name', 'genre__name', 'movielist_score')
        return Response(article[0])
    else:
        return Response({'error':'movie {:s} not found'.format(pk)}, status=404)

@api_view(['POST'])
def addArticle(request):
    popularity = request.POST.get("popularity", None)
    director = request.POST.get("director", None)
    genre_id = request.POST.get("genre_id", None)
    movie = Movies.objects.create(popularity=popularity,
                                     director=director,
                                     genre_id = genre_id,
                                     name=request.FILES['name'])

    return Response({'message': 'article {:d} movielist_score'.format(article.id)}, status=301)
