from django.shortcuts import render
from django.http import HttpResponseNotFound

from rest_framework.response import Response
from rest_framework.decorators import api_view

from articles.models import Article
# Create your views here.
def test(request):
    return render(request, 'test.html', {})

def latest(request):
    this_article = Article.objects.first()
    if this_article:
        context = {
            'heading':this_article.heading,
            'body' : this_article.body,
            'reporter' : this_article.reporter.name if this_article.reporter else '',
            'created' : this_article.created,
            'image' : this_article.image.url if this_article.image else None
        }
        return render(request, 'all.html', context)
    else:
        return HttpResponseNotFound("Article not found")

def article_details(request, pk):
    #pk =1
    ###print article_id
    this_article = Article.objects.filter(id=pk).first()
    if this_article:
        context = {
            'heading':this_article.heading,
            'body' : this_article.body,
            'reporter' : this_article.reporter.name if this_article.reporter else '',
            'created' : this_article.created,
            'image' : this_article.image.url if this_article.image else False
        }
        return render(request, 'all.html', context)
    else:
        return HttpResponseNotFound("Article not found")


@api_view(['GET'])
def allArticles(request):
    all_articles = Article.objects.all().values()
    return Response(all_articles)

@api_view(['GET'])
def articleDetails(request, pk):
    if Article.objects.filter(pk=pk).exists():
        article = Article.objects.filter(pk=pk).values('heading', 'body', 'image', 'reporter__name', 'created')
        return Response(article[0])
    else:
        return Response({'error':'article {:s} not found'.format(pk)}, status=404)

@api_view(['POST'])
def addArticle(request):
    heading = request.POST.get("heading", None)
    body = request.POST.get("body", None)
    reporter_id = request.POST.get("reporter_id", None)
    article = Article.objects.create(heading=heading,
                                     body=body,
                                     reporter_id = reporter_id,
                                     image=request.FILES['image'])

    return Response({'message': 'article {:d} created'.format(article.id)}, status=301)
