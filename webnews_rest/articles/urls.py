from django.conf.urls import include, url
from .import views

urlpatterns = [
    url(r'^test$', views.test),
    url(r'^all$', views.allArticles),
    url(r'^latest$', views.latest),
    url(r'^(?P<pk>\d+)$', views.articleDetails),
    url(r'^add$', views.addArticle),
    #url(r'article/(?P<pk>[0-9]+)/$', views.article_details)

]
